
# Uncomment this and set it to your proxy address if needed
#http_proxy=http://proxy.latrobe.edu.au:8080

# Either galaxy-dist (stable) or galaxy-central
# 
galaxy_branch='galaxy-dist'

# Replace with your email address
# Use this email address when registering as a user in the galaxy interface
admin_email='iracooke@gmail.com'

# In general you shouldn't need to edit this
#
protkversion='1.2.2'

if [[ -n $http_proxy ]]; then

	sudo touch /etc/apt/apt.conf
	sudo chmod o+w /etc/apt/apt.conf
	sudo echo "Acquire::http::Proxy \"$http_proxy\";" > /etc/apt/apt.conf
	sudo chmod o-w /etc/apt/apt.conf

	export http_proxy
	export https_proxy=$http_proxy
fi

sudo apt-get update
#sudo apt-get upgrade -y

sudo apt-get install -y curl build-essential mercurial libreadline6-dev zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev autoconf libgdbm-dev libncurses5-dev automake libtool bison pkg-config libffi-dev subversion libbz2-dev swig expat libpng12-dev gawk gnuplot libperl-dev libfuse-dev libcurl4-openssl-dev libxml2-dev libgd2-xpm-dev liblocal-lib-perl cpanminus unzip openjdk-7-jre

if [ ! -f /home/vagrant/virtualenv.py ]; then
	su vagrant -c "wget https://bitbucket.org/ianb/virtualenv/raw/tip/virtualenv.py"
fi

if [ ! -d /home/vagrant/galaxy_env ]; then
	su vagrant -c "python virtualenv.py --no-site-packages galaxy_env"
	su vagrant -c "echo '. ~/galaxy_env/bin/activate' >> ~/.bashrc"
fi

if [ ! -d /home/vagrant/$galaxy_branch ]; then
	if [ -d /vagrant/$galaxy_branch ]; then
		su vagrant -c "cp -r /vagrant/$galaxy_branch ./"
	else
		su vagrant -c "hg clone https://bitbucket.org/galaxy/$galaxy_branch"
		su vagrant -c "cd $galaxy_branch"
		su vagrant -c "hg update stable"
		cd ..
	fi
fi

if [ ! -d /home/vagrant/.protk/tmp ]; then
	if [ -d /vagrant/payload ]; then
		# To ensure that downloads are speeded up
		su vagrant -c "mkdir -p ~/.protk/tmp/"
		su vagrant -c "cp -r /vagrant/payload/* ~/.protk/tmp/; cp /vagrant/payload/universe_wsgi.ini ~/$galaxy_branch/"
	else
		su vagrant -c "curl -L https://get.rvm.io | bash -s -- --ignore-dotfiles"
		rvm_bin=/home/vagrant/.rvm/bin/rvm
		su vagrant -c "$rvm_bin install --autolibs=2 1.9.3"

		su vagrant -c "$rvm_bin 1.9.3 do rvm gemset create protk-$protkversion"

		su vagrant -c "$rvm_bin 1.9.3@protk-$protkversion do gem install --version '>=1.2.4' protk --no-ri --no-rdoc"
		su vagrant -c "$rvm_bin 1.9.3@protk-$protkversion do protk_setup.rb pwiz"
		su vagrant -c "$rvm_bin 1.9.3@protk-$protkversion do protk_setup.rb tpp"
		su vagrant -c "$rvm_bin 1.9.3@protk-$protkversion do protk_setup.rb blast"
		su vagrant -c "$rvm_bin 1.9.3@protk-$protkversion do protk_setup.rb msgfplus"	
	fi
fi

if [ ! -f /home/vagrant/$galaxy_branch/universe_wsgi.ini ]; then
	cd /home/vagrant/$galaxy_branch
	su vagrant -c "cp universe_wsgi.ini.sample universe_wsgi.ini"
	sed -i.bak s/'#host = 127.0.0.1'/'host = 0.0.0.0'/ universe_wsgi.ini
	sed -i.bak s%'#tool_dependency_dir = None'%'tool_dependency_dir = ../tool_dependencies'% universe_wsgi.ini
	sed -i.bak s%'#tool_config_file = tool_conf.xml,shed_tool_conf.xml'%'tool_config_file = tool_conf.xml,shed_tool_conf.xml'% universe_wsgi.ini
	admin_users_string="admin_users = "$admin_email
	sed -i.bak "s%#admin_users = None%$admin_users_string%" universe_wsgi.ini
fi


