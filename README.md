## Setup

Setup a lightweight VM running Galaxy ready to install Proteomics tools from the galaxy toolshed

1. Install [Virtualbox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/)

2. Clone this repository

		git clone git@bitbucket.org:iracooke/protk_vagrant.git

3. Edit `bootstrap.sh`.  In particular, you should change `admin_email` to your email address

4. Start the virtual machine (this will take a while)

		cd protk_vagrant
		vagrant up

5. Login to the newly created VM

		vagrant ssh

6. Start galaxy on the VM

		cd galaxy-dist
		sh run.sh --reload

7. Access Galaxy from your host machine by navigating to `localhost:8080`

8. Register as a new user with the email address provided in step 3

9. Navigate to the Galaxy _Admin_ tab and choose the _search and browse toolsheds_ option. Your VM should have the necessary dependencies installed to allow installation of Proteomics tools in the protk family from the main galaxy toolshed.  This includes the following repositories

	**mascot,
	tpp_prophets,
	xtandem,
	msgfplus,
	omssa**

All repositories with names that begin with protk\_ are simply dependency installers these should be installed automatically as a result of installing one of the repositories named above.

## Troubleshooting

At the present time the galaxy dependency installation framework cannot easily handle installation of dependencies with long compile times.  The first rule to dealing with this is to exercise patience ... wait at least an hour before deciding that an installation hasn't worked.  Nevertheless it is possible that the **protk\_trans\_proteomic\_pipeline** package on the galaxy toolshed may fail installation on the first try.  If this occurs you should uninstall the protk_trans_proteomic_pipeline package and then reinstall it. 
